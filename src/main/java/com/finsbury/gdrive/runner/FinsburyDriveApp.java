package com.finsbury.gdrive.runner;

import java.io.IOException;
import com.finsbury.gdrive.engine.GoogleDriveDirectoryGenerator;
import com.finsbury.gdrive.exceptions.GoogleDriveInputException;

/**
 * EntryPoint to the Finsbury Google Drive Directory enumeration application
 * @author slasisi
 *
 */
public class FinsburyDriveApp {
	
	
	public static void main(String[] args) throws IOException, GoogleDriveInputException {
	        GoogleDriveDirectoryGenerator.generateDirectoryStructure();
	        
	
	}
}