package com.finsbury.gdrive.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility class for writing string text to files on disk.
 * @author slasisi
 *
 */
public class UtilityWriter {
	
	/**
	 * Writes a file to disk given a StringBuilder and a given titlePrefix. <br />
	 * Generates a  strong/secured random number alongside the current/Date time <br />
	 * to build the filename
	 * @param stringBuilder The sting object to write to disk
	 * @param titlePrefix The prefix of the filename
	 * @return the written file object
	 * @throws IOException
	 */
	public static File writeFile(StringBuilder stringBuilder, String titlePrefix) throws IOException {
			String currentTimeStamp = new SimpleDateFormat("dd_MM_yyyy_hhmm_ss").format(new Date());
			int randomInteger= (new SecureRandom().nextInt(100))+1;
			StringBuilder fileNameSb = new StringBuilder().append(titlePrefix).
					append(currentTimeStamp).
					append(Integer.toString(randomInteger)).
					append(".txt");						
			java.io.File file = new java.io.File(fileNameSb.toString());
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(stringBuilder.toString());
			bw.close();
			return file;
	}
	
}