package com.finsbury.gdrive.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.api.client.util.Strings;

/**
 * Utility class to Retrieve properties from a configuration properties file
 * @author slasisi
 *
 */
public class PropertyRetriever {
	
	static final Properties properties = new Properties();
	private static final Logger logger = LoggerFactory.getLogger(PropertyRetriever.class);
	
	static{
				try {
					properties.load(new FileInputStream("config/config.properties"));
				} catch (IOException e) {
					logger.error("Error loading configuration properties",e);
				}
			
	}
	
	/**
	 * Retrieves the integer property of the given propertyName
	 * @param propertyName
	 * @return
	 */
	public  static int getIntegerProperty(String propertyName){
		String property = properties.getProperty(propertyName);
		return !Strings.isNullOrEmpty(property) ? Integer.parseInt(property) : -1;
	}
	
	/**
	 * Retrieves the string property of the given propertyName
	 * @param propertyName
	 * @return
	 */
	public  static String getProperty(String propertyName){
		return properties.getProperty(propertyName);
	}
	

}
