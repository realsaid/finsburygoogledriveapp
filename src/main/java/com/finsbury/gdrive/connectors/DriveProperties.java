package com.finsbury.gdrive.connectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.finsbury.gdrive.utils.PropertyRetriever;
import com.google.api.client.repackaged.com.google.common.base.Strings;

/**
 * Retrieves specific Configuration properties from an external but dynamic property configuration file<br />
 * These properties are peculiar to this solution for integrating with the Google Drive API
 * @author slasisi
 *
 */
public class DriveProperties {
	
	private static final int MAX_FILE_NAME_LEN = 260;

	private static final Logger logger = LoggerFactory.getLogger(DriveProperties.class);
	
	private static final String DEFAULT_REPORT_PREFIX = "Report_";
	private static final String DEFAULT_REPORTS_DIR = "Reports";
	private static final int DEFAULT_PAGE_SIZE = 100;
	private static final int MAX_PAGE_SIZE = 50000;
	private static final int DEFAULT_TRANSVERSAL_DEPTH_LIMIT = 5;
	private static final int MAX_TRANSVERSAL_DEPTH_LIMIT = 100;

	public static int getDirectoryTransversalLimit(){
		int transversalLimit = PropertyRetriever.getIntegerProperty("transversal_depth_limit");
		if(transversalLimit == -1){
			logger.warn("Cannot retrieve Directory Transversal limit, will now switch to default");
			return DEFAULT_TRANSVERSAL_DEPTH_LIMIT;
		}
		else if(transversalLimit > MAX_TRANSVERSAL_DEPTH_LIMIT){
			logger.warn("Transversal limit can't be deeper than the MAXIMUM DEPTH VALUE" +
						MAX_TRANSVERSAL_DEPTH_LIMIT+
						", will now switch to default!");
			return DEFAULT_TRANSVERSAL_DEPTH_LIMIT;
		}
		return transversalLimit;
	}
	
	public static int getDefaultPageSize(){
		int defaultPageSize = PropertyRetriever.getIntegerProperty("default_page_size");
		if(defaultPageSize == -1){
			logger.warn("Cannot retrieve default page size, will now switch to default");
			return DEFAULT_PAGE_SIZE;
		}
		else if(defaultPageSize > MAX_PAGE_SIZE){
			logger.warn("Specified page size can't be greater than the MAXIMUM PAGE SIZE VALUE, "
					+ "will now switch to default!");
			return DEFAULT_PAGE_SIZE;
		}
		return defaultPageSize;
	}
	
	public static String getReportsDirectory(){
		String reportsDirectory = PropertyRetriever.getProperty("reports_dir");
		if(Strings.isNullOrEmpty(reportsDirectory) || 
				reportsDirectory.length() > MAX_FILE_NAME_LEN){
			logger.warn("Empty fileName entry or sepcified file name is too long, switching to default!");
			return DEFAULT_REPORTS_DIR;
		}
			    
		return reportsDirectory;
	}
	
	public static String getReportPrefix(){
		String reportPrefix = PropertyRetriever.getProperty("report_prefix");
		if(reportPrefix.length() > MAX_FILE_NAME_LEN){
			logger.warn("Specified file name prefix is too long, switching to default! ");
			return DEFAULT_REPORT_PREFIX;
		}
		else if(Strings.isNullOrEmpty(reportPrefix)){
			return "";
		}
		return reportPrefix;
	}
	
}
