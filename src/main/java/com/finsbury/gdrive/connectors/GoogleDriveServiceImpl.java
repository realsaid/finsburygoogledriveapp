package com.finsbury.gdrive.connectors;
import java.io.IOException;

import com.finsbury.gdrive.services.GoogleDriveService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;

/**
 * 
 */

/**
 * @author slasisi
 *
 */
public class GoogleDriveServiceImpl implements GoogleDriveService {
	
		/** Google App Credential. */
	 	private  final Credential credential;
	 	
	 	/** Application name. */
	    private static final String APPLICATION_NAME =
	        "Finsbury Drive Service";

	    /** Global instance of the JSON factory. */
	    private static final JsonFactory JSON_FACTORY =
	        JacksonFactory.getDefaultInstance();

	    /** Global instance of the HTTP transport. */
	    private static HttpTransport HTTP_TRANSPORT;
	    
	    static {
	        try {
	            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
	        } catch (Throwable t) {
	            t.printStackTrace();
	            System.exit(1);
	        }
	    }
	 
	 public GoogleDriveServiceImpl(Credential credential){
		 this.credential = credential;
	 }
	 
	 
	 /* (non-Javadoc)
	 * @see com.finsbury.gdrive.connectors.GoogleDriveServiceContract#getDriveService()
	 */
    @Override
	public Drive getDriveServiceAPI() throws IOException {
        return new Drive.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }
}
