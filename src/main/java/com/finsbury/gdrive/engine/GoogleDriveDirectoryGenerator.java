package com.finsbury.gdrive.engine;

import com.google.api.services.drive.model.*;
import com.finsbury.gdrive.exceptions.GoogleDriveInputException;
import com.finsbury.gdrive.services.GoogleDriveService;
import com.finsbury.gdrive.utils.UtilityWriter;
import com.google.api.services.drive.Drive;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.finsbury.gdrive.connectors.*;

/**
 * Implements the core operations of the Google Drive Directory transversal and Enumeration.
 * Aggregates different services to walk through the Drive, memorize the structure and <br />
 * render it as a report on the remote drive.
 * @author slasisi
 *
 */
public class GoogleDriveDirectoryGenerator {

	private static final Logger logger = LoggerFactory.getLogger(GoogleDriveDirectoryGenerator.class);
	private static final String FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";
    private static final String FOLDER_TYPE_QUERY = "mimeType = 'application/vnd.google-apps.folder'";
    private static final String ROOT_TYPE_QUERY = "'root' in parents and trashed=false";
	private static final String REPORT_PREFIX = DriveProperties.getReportPrefix();
	private static final String REPORTS_DIR = DriveProperties.getReportsDirectory();
	private static final int DEFAULT_PAGE_SIZE = DriveProperties.getDefaultPageSize();
	private static final int TRANSVERSAL_DEPTH_LIMIT = DriveProperties.getDirectoryTransversalLimit();
    private static int recursion_depth = 0;
    private static  Drive serviceAPI;
    private static String reportFolderId = null;
    private static StringBuilder dirStructureString;
    private static GoogleDriveFileCreator driveFileCreator;
    private static GoogleDriveUploader driveFileUploader;
    
    /**
     * Generates the directory structure of the Google Drive of the specified user <br />
     * based on the authorized GoogleDrive Credential
     * @throws IOException
     * @throws GoogleDriveInputException
     */
	public static void generateDirectoryStructure() throws IOException, GoogleDriveInputException {
        // Build a new authorized API client service.
		 GoogleDriveService googleDriveService = 
				 new GoogleDriveServiceImpl(GoogleDriveCredential.authorize());
         serviceAPI = googleDriveService.getDriveServiceAPI();
         dirStructureString = new StringBuilder();
         driveFileCreator = new GoogleDriveFileCreator();
         driveFileUploader = new GoogleDriveUploader();
       // Print the names of files in the root Directory
         dirStructureString.append("Root Directory : \n");
       processRootFiles(serviceAPI);
       processFolders(serviceAPI);
       if(reportFolderId == null) {
    	   driveFileCreator
    	   .createFile(serviceAPI, REPORTS_DIR, FOLDER_MIME_TYPE);
			}
      
       driveFileUploader.uploadToDrive(
    		   serviceAPI,
    		   UtilityWriter.writeFile(dirStructureString, REPORT_PREFIX), 
    		   reportFolderId
    		   );
       
    }

	private static void processRootFiles(Drive service) throws IOException {
		processFiles(service.files().list()
		    		  .setQ(ROOT_TYPE_QUERY)
		              .setPageSize(DEFAULT_PAGE_SIZE)
		              .setFields("nextPageToken, files(id, name)")
		              .execute());
	}

	/**
	 * @param result
	 * @throws IOException 
	 */
	private static void processFiles(FileList result) throws IOException {
		List<File> files = result.getFiles();
        if (files == null || files.size() == 0) {
        	logger.info("No files found.");
        } else {
            for (File file : files) {
            	 if(REPORTS_DIR.equals(file.getName())){
                 	reportFolderId = file.getId();
                 }
            	 if(file.getMimeType() != null &&
                         file.getMimeType().contains(FOLDER_MIME_TYPE) 
                         && recursion_depth < TRANSVERSAL_DEPTH_LIMIT ) {
      		                	dirStructureString.append("\n")
      		                	.append(file.getName())
      		                	.append("\nDirectory Listing :\n");
      			                processFiles(getChildren(file));
      			                recursion_depth++;
                      }
            	 else{
            		 dirStructureString.append(file.getName()).append("\n");
            	 }
                
            }
        }
	}
	
	/**
	 * Retrieves folders from the root directory and then visits their sub-directories<br />
	 * up to the depth of the previously defined search depth limit.
	 * @param service The Drive API service
	 * @throws IOException 
	 */
	private static void processFolders(Drive service) throws IOException {
		List<File> folders = service.files().list()
								    .setQ(FOLDER_TYPE_QUERY)
								    .execute()
								    .getFiles();
        if (folders == null || folders.size() == 0) {
        	logger.info("No folders found.");
        } else {
        	//Find the root directory
            logger.info("Enumerating Directory Structure");
            for (File file : folders) {
            	dirStructureString.append("\n").append(file.getName())
            	.append("\nDirectory Listing :\n");
            	processFiles(getChildren(file));
            }
        }
	}
	
	/**
	 * Gets children of a given google drive file folder
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private static FileList getChildren(File file) throws IOException {
		StringBuilder queryBuilder = new StringBuilder(100);
		queryBuilder.append("'").
		append(file.getId()).
		append("' in parents");
		return serviceAPI.files().list().setQ(queryBuilder.toString()).execute();
	}
	
	
	
	

}