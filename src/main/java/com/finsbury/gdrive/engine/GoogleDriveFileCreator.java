package com.finsbury.gdrive.engine;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.finsbury.gdrive.exceptions.GoogleDriveInputException;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.File;
import com.google.common.base.Strings;

/**
 * Implements the creation of a file in Google Drive
 * @author slasisi
 *
 */
public class GoogleDriveFileCreator
{
	private static final Logger logger = 
			LoggerFactory.getLogger(GoogleDriveFileCreator.class);
	
	/**
	 * Creates a file of a given content type in google drive using the Google Drive API service
	 * @param service The Google API service
	 * @param fileName 
	 * @param mimeType The content type of the file
	 * @return the create
	 * @throws IOException
	 * @throws GoogleDriveInputException
	 */
	 public File createFile(Drive service, String fileName, String mimeType)
			 throws IOException, GoogleDriveInputException{
		 if(service == null || Strings.isNullOrEmpty(fileName)
				 || Strings.isNullOrEmpty(mimeType)){
				throw new GoogleDriveInputException
				("A valid service, file name and mime content type is required to create a file");
			}
			File fileMetadata = new File();
			fileMetadata.setName(fileName);
			fileMetadata.setMimeType(mimeType);	
			Files files = service.files();
			if(files==null){
				throw new IOException("Can not create files from the given service!");
			}
			File file = files.create(fileMetadata)
				        .setFields("id, name")
				        .execute();
			logger.info("File of type %s created successfully with id : %s",mimeType,file.getId());
			return file;
		}
}