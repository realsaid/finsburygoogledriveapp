package com.finsbury.gdrive.engine;

import java.io.IOException;
import java.util.Collections;

import com.finsbury.gdrive.exceptions.GoogleDriveInputException;
import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.File;
import com.google.common.base.Strings;

/**
 * Implements file upload to Google Drive using the Drive Service API
 * @author slasisi
 */
public class GoogleDriveUploader {
	
	
	/**
	 * Uploads a single file into a destination parent folder <br />
	 * on Google drive using the Drive Service API
	 * @param service the drive service API
	 * @param file the file to be uploaded
	 * @param parentFolder the destination parent folder, <br/>
	 * 		   if null the root of Google drive will be the default destination
	 * @throws IOException thrown when an input/output or writing error occurs before/during upload
	 * @throws GoogleDriveInputException thrown any of the parameters needed is null or empty
	 */
	 public File uploadToDrive(Drive service, java.io.File file, String parentFolder) 
			 throws IOException, GoogleDriveInputException {
		if(service == null || file == null){
			throw new GoogleDriveInputException
			("All upload resources must be available i.e. Drive service and file ");
		}
		File fileMetadata = new File();
		fileMetadata.setName(file.getName());
		if(!Strings.isNullOrEmpty(parentFolder)){//Parent Folder is the root when destination is null
			fileMetadata.setParents(Collections.singletonList(parentFolder));
		}
		FileContent dataContent = new FileContent("text/plain", file);
		//Upload file
		
		Files files = service.files();
		if(files==null){
			throw new IOException("Can not create files from the given service!");
		}
		File uploadFile = files.create(fileMetadata,dataContent)
						  .setFields("id, parents")
						  .execute();
		System.out.printf("Report file : %s with file id: %s uploaded successfully!!!",
				file.getName(),uploadFile.getId());
		//delete file locally
		file.delete();
		return uploadFile;
	}

}
