package com.finsbury.gdrive.exceptions;

/**
 * Thrown when a required input parameter for the Drive API is missing or null 
 * @author slasisi
 */
public class GoogleDriveInputException extends Exception {

	private static final long serialVersionUID = 1L;

	public GoogleDriveInputException(String message) {
		super(message);
	}

}
