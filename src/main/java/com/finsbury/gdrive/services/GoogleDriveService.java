package com.finsbury.gdrive.services;

import java.io.IOException;

import com.google.api.services.drive.Drive;

public interface GoogleDriveService {

	/**
	 * Build and return an authorized Drive client service.
	 * @return an authorized Drive client service
	 * @throws IOException
	 */
	Drive getDriveServiceAPI() throws IOException;

}