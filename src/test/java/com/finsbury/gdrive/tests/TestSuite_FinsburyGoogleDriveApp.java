package com.finsbury.gdrive.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({Test_GoogleDriveCredential.class, Test_GoogleDriveFileCreator.class,
		Test_GoogleDriveUploader.class, Test_Integrate_GoogleDriveService.class, Test_MockGoogleDriveService.class })
public class TestSuite_FinsburyGoogleDriveApp {

}
