package com.finsbury.gdrive.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import com.finsbury.gdrive.engine.GoogleDriveFileCreator;
import com.finsbury.gdrive.engine.GoogleDriveUploader;
import com.finsbury.gdrive.exceptions.GoogleDriveInputException;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

public class Test_GoogleDriveFileCreator {
	
	private GoogleDriveFileCreator googleDriveFileCreator;
	private Drive driveService;
	private File googleFile;

	@Before
	public void setUpMock()  throws Exception {
		driveService = PowerMockito.mock(Drive.class);
		googleDriveFileCreator = PowerMockito.mock(GoogleDriveFileCreator.class);
		googleFile = new File();
	}

	@After
	public void tearDown() throws Exception {
		driveService = null;
		googleDriveFileCreator = null;
		googleFile = null;
	}

	@PrepareForTest({ GoogleDriveUploader.class })
	@Test
	public final void testCreateFile() throws IOException, GoogleDriveInputException {
		PowerMockito.when(googleDriveFileCreator
				.createFile(driveService, "Reports_1784238", "text/plain")).thenReturn(googleFile);
		assertEquals(googleDriveFileCreator
				.createFile(driveService, "Reports_1784238", "text/plain") , googleFile);
	}
	
	@PrepareForTest({ GoogleDriveUploader.class })
	@Test(expected=GoogleDriveInputException.class)
	public final void testCreateFile_shouldThrowException() throws IOException, GoogleDriveInputException {
		googleDriveFileCreator = new GoogleDriveFileCreator();
		googleDriveFileCreator.createFile(driveService, null, "text/plain");
		googleDriveFileCreator.createFile(null, null, "");
	}
	
	@Test(expected=IOException.class)
	public final void testCreateFile_shouldThrowIOException() throws IOException, GoogleDriveInputException {
		googleDriveFileCreator = new GoogleDriveFileCreator();
		googleDriveFileCreator
		.createFile(driveService, "Reports_1784238", "text/plain");
	}



}
