package com.finsbury.gdrive.tests;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.finsbury.gdrive.engine.GoogleDriveUploader;
import com.finsbury.gdrive.exceptions.GoogleDriveInputException;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;


@RunWith(PowerMockRunner.class)
public class Test_GoogleDriveUploader {

	private GoogleDriveUploader googleDriveUploader;
	private Drive driveService;
	private java.io.File file;
	private File googleFile;
	
	@Before
	public void setUpMock() throws Exception {
		driveService = PowerMockito.mock(Drive.class);
		file = PowerMockito.mock(java.io.File.class);
		googleDriveUploader = PowerMockito.mock(GoogleDriveUploader.class);
		googleFile = new File();
	}

	@After
	public void tearDown() throws Exception {
		driveService = null;
		file = null;
		googleDriveUploader = null;
		googleFile = null;
	}

	@PrepareForTest({ GoogleDriveUploader.class })
	@Test
	public final void testUploadToDrive() 
			throws IOException, GoogleDriveInputException {
		PowerMockito.when(googleDriveUploader
				.uploadToDrive(driveService, file, "Reports"))
				.thenReturn(googleFile);
		assertEquals(googleDriveUploader.
						uploadToDrive(driveService, file, "Reports") , 
						googleFile);
	}
	
	@Test(expected=GoogleDriveInputException.class)
	public final void testUploadToDrive_shouldThrowGoogleDriveException() 
			throws IOException, GoogleDriveInputException {
		googleDriveUploader = new GoogleDriveUploader();
		googleDriveUploader.uploadToDrive(driveService, null, "Reports");
		googleDriveUploader.uploadToDrive(null, null, "");
	}
	
	@Test(expected=IOException.class)
	public final void testUploadToDrive_shouldThrowIOException() 
			throws IOException, GoogleDriveInputException {
		googleDriveUploader = new GoogleDriveUploader();
		googleDriveUploader.uploadToDrive(driveService, file, "Reports");
	}


}
