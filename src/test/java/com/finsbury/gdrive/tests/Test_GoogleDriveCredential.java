package com.finsbury.gdrive.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.finsbury.gdrive.connectors.GoogleDriveCredential;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.json.jackson2.JacksonFactory;

public class Test_GoogleDriveCredential {

	private static final String trustedTransport = 
								"com.google.api.client.http.javanet.NetHttpTransport@";
	
	private static Credential testCredential;
	
	@Before
	public final void setUp() throws IOException{
		 testCredential = GoogleDriveCredential.authorize();
	}
	
	@After
	public final void tearDown() throws IOException{
		 testCredential = null;
	}

	@Test
	public final void testAuthorize() throws IOException, GeneralSecurityException {
		assertNotNull(testCredential);
	}
	
	@Test
	public final void testJsonFactory() throws IOException, GeneralSecurityException {
		assertEquals(testCredential.getJsonFactory() , JacksonFactory.getDefaultInstance());
	}
	
	@Test
	public final void testTrustedTransport() throws IOException, GeneralSecurityException {
		assertTrue(testCredential.getTransport().toString().contains(trustedTransport));
	}
	
	

}
