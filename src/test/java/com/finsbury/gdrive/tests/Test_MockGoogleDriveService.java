package com.finsbury.gdrive.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.finsbury.gdrive.services.GoogleDriveService;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.About;
import com.google.api.services.drive.Drive.About.Get;

@RunWith(PowerMockRunner.class)
public class Test_MockGoogleDriveService {
	@Mock
	GoogleDriveService driveService;
	@Mock
	Drive drive;
	@Mock
	About about;
	@Mock
	Get get;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() throws Exception {
		driveService = null;
	}

	@Test
	public final void testGoogleDriveService() {
		assertNotNull(driveService);
		assertTrue(driveService instanceof GoogleDriveService);
	}

	@Test
	public final void testGetDriveService() throws IOException {
		PowerMockito.when(driveService.getDriveServiceAPI()).thenReturn(drive);
		Drive mockDrive = driveService.getDriveServiceAPI();
		assertEquals(mockDrive, drive);
		assertTrue(mockDrive instanceof Drive);
	}
	
	@Test
	public final void testGetDriveService_Authentication() throws IOException {
		PowerMockito.when(driveService.getDriveServiceAPI()).thenReturn(drive);
		PowerMockito.when(drive.about()).thenReturn(about);
		PowerMockito.when(about.get()).thenReturn(get);
		PowerMockito.when(get.execute()).thenAnswer(metadataResponseStub());
		//Should fail if it can't authenticate
	    assertNotNull(driveService.getDriveServiceAPI().about().get().execute());
	}

	private Answer<com.google.api.services.drive.model.About> metadataResponseStub() {
		return new Answer<com.google.api.services.drive.model.About>() {
		     public com.google.api.services.drive.model.About answer(InvocationOnMock invocation) throws Throwable {
		         return new com.google.api.services.drive.model.About();
		     }
		};
	}

}
