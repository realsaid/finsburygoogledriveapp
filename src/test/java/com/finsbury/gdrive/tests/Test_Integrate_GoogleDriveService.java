package com.finsbury.gdrive.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.finsbury.gdrive.connectors.GoogleDriveCredential;
import com.finsbury.gdrive.connectors.GoogleDriveServiceImpl;
import com.finsbury.gdrive.services.GoogleDriveService;
import com.google.api.services.drive.Drive;


public class Test_Integrate_GoogleDriveService {
	
	GoogleDriveService driveService;

	@Before
	public void setUp() throws Exception {
		driveService = new GoogleDriveServiceImpl(GoogleDriveCredential.authorize());
	}

	@After
	public void tearDown() throws Exception {
		driveService = null;
	}

	@Test
	public final void testGoogleDriveService() {
		assertTrue(driveService instanceof GoogleDriveService);
	}

	@Test
	public final void testGetDriveService() throws IOException {
		assertTrue(driveService.getDriveServiceAPI()
				    instanceof
				    Drive);
	}
	
	@Test
	public final void testGetDriveService_Authentication() throws IOException {
		//Should fail if it can't authenticate
	    assertNotNull(driveService.getDriveServiceAPI().about().get());
	}

}
