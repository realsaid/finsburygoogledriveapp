Finsbury Google Drive Enumeration App
===

Scenario
---
Finsbury Foods is a wholesaler located in London. Finsbury two years ago decided to consolidate all of their documents inside Google Drive. They would however like to generate a report of their google drive directory structure viewed as a specific user. They have come to Cloudreach for a software solution.

Finsbury Foods would like an overview of the architecture that will be used for this software solution and a document that will describe how the solution will be implemented.

Finsbury Foods would like to run the program in the background on a Linux server and have the generated document saved to a folder called �Reports� inside of the Finsbury Foods Google Drive. The document should have a name which identifies the date on which it was created.


**Requirements**
1. Implementation of a Linux based background application that generates a report of Finsbury Google Drive directory structure tied to a specific user account and stores the report in a reports folder on Google Drive.

2. An architectural overview of the solution from a client's point of view.

3. Installation and usage instruction of the application


Instructions
-------------------
Install Gradle on your linux machine if you don't have it already installed on your machine.
```
 sudo add-apt-repository ppa:cwchien/gradle; sudo apt-get update ; sudo apt-get install gradle
```

Finally, source your .profile and test gradle.

 - source ~/.profile

 - which gradle

 - gradle -version
 
 
**Install Finsbury Application **
 - Unzip the zip package of the application in your preferred directory
```
unzip FinsburyGoogleDriveApp.zip
```
** Generate your own client and Turn on the Drive API**
 - Turn on the Drive API
    follow the instructions on the link https://console.developers.google.com/start/api?id=drive
    
 - Download the JSON Secret Key and rename it as client_secret.json, then copy or overwrite existing to the application directory: 
 FinsburyGoogleDriveApp/config/client_secret.json
	
 - If you have any configuration settings to modify or tweak, you can edit the :
FinsburyGoogleDriveApp/config/config.properties
***Then build an executable jar by running the command ****
```
gradle clean; gradle createExecJar;
```
	 	
 ** Build,package and run the application with the command from the application directory **
 ```
gradle -q run or java -jar build/libs/FinsburyGoogleDriveApp-all-1.0.jar
```


***Key based Oauth Authorization***
The first time you run the app, it will prompt you to authorize access via OAuth2 (which requires a browser prompt for authorizing
Scope of access and authentication):

It will attempt to open to open a new window or tab in your default browser. If this fails, copy the URL from the console and manually open it in your browser.

If you are not already logged into your Google account, you will be prompted to log in. If you are logged into multiple Google accounts, you will be asked to select one account to use for the authorization.
Click the Accept button.
Afterwards and subsequently, this application will proceed automatically.
